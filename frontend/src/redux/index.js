// this is app level mutations, can be added at app level
export const appReducer = (state, action) => {
    switch (action.type) {
          case 'errorInGettingLoans':
            return Object.assign({}, state, {authState: {loggedin: true}, loans: [], message: 'Error in fetching loans data' });
          case 'setLoansData':
            return Object.assign({}, state, {authState: {loggedin: true}, loans: action.data.loans.body, message: '' });
          case 'setUser':
            return Object.assign({}, state, {authState: {loggedin: true}, authenticatedUser: action.data.user, message: action.data.message });
          case 'invalidCredentials':
            return Object.assign({}, state, {authState: {loggedin: false}, authenticatedUser: null, message: action.data.message });
          default:
            return state;
    }
}
