import {createStore} from 'redux';
import {appReducer} from './index';

export function makeStore() {
  return createStore(appReducer, { 
    authState: {
      loggedin: false,
    }, 
    authenticatedUser: null,
    loans: [] 
  });
}
