import request from 'request';

const API_URL = `https://virtserver.swaggerhub.com/i6643/lendSomeMoolah/1.0.0`;
const MOCK_USERS_DATA = [ 
    {
        "username": "davyJones",
        "password": "n9G%YE6nnhWG",
        "role": "lender"
    },
    {
        "username": "jackTaylor",
        "password": "vhU51&17XZaW",
        "role": "borrower"
    },
    {
        "username": "captainSparrow",
        "password": "76%Q7xOG3nPK",
        "role": "lender"
    },
    {
        "username": "johnGibbs",
        "password": "P0$6nq4z2AKN",
        "role": "borrower"
    }
];

// logic to get matching user details from MOCK DB (above JSON array)
const getUserDetails = ({enteredUsername, enteredPassword}) => {
    const matchingUserArr = MOCK_USERS_DATA.filter((userDetails) => ((enteredUsername === userDetails.username) && (enteredPassword === userDetails.password)));
    return (matchingUserArr.length > 0) && matchingUserArr[0];
}

export const getLoansForUser = (dispatch, { username }) => {
    const options = {
        method: 'GET',
        url: `${API_URL}/loans/${username}`,
        headers: {accept: 'application/json'}
    };
    
    request(options, function (error, response, body) {
        console.log(JSON.parse(body));
        if (error) {
            dispatch({ type: 'errorInGettingLoans', data: { message: ' error in fetching loans data '} });
        } else {
            dispatch({ type: 'setLoansData', data: { loans: { body: JSON.parse(body) }, message: ''} });
        }
    });
}

// action to do login
export const doSignup = async (dispatch, data) => {
    const user = getUserDetails(data);
    if (user) {
        dispatch({ type: 'setUser', data: { user, message: ''} });
    } else {
        dispatch({ type: 'invalidCredentials', data: { message: 'credentials were invalid. try again' } });
    }
}
