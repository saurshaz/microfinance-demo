import React from 'react';

import "react-simple-flex-grid/lib/main.css";

/*
    Component For TabularData
*/
export const TabularData = (props) => {
    const { renderTableData, renderTableHeadings, heading } = props;

    return (
        <section>
            {heading}
            {renderTableHeadings}
            {renderTableData}
        </section>
    );
}