import React from 'react';
import { useDispatch, useMappedState } from 'redux-react-hook';
import { BrowserRouter as Router, Switch, Route, Redirect, Link } from 'react-router-dom';
import { Dashboard } from './pages/Dashboard';
import { Login } from './pages/Login';
import { Home } from './pages/Home';

export const App = (props) => {
    const mapState = React.useCallback((state) => state, []);
    const dispatch = useDispatch();
    const appState = useMappedState(mapState);

    const decorateProps = (props) => {
        return Object.assign(props, {}, { dispatch, appState });
    }
    return (
        <Router>
            <section>
                <nav>
                    <ul>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/dashboard">Dashboard</Link>
                        </li>
                    </ul>
                </nav>
                <div style={{ padding: "2%" }}>
                    <h3>{appState.message}</h3>
                    <Switch>
                        <Route
                            exact
                            path="/login"
                            render={(props) => (<Login {...decorateProps(props)} />)} />
                        <Route
                            exact
                            path="/"
                            render={(props) =>
                                (appState.authState.loggedin) ? (<Home {...decorateProps(props)} />)
                                    : (<Redirect to={{ pathname: '/login', state: { from: props.location } }} />)
                            } />
                        <Route
                            exact
                            path="/dashboard"
                            props={appState}
                            render={(props) =>
                                (appState.authState.loggedin) ? (<Dashboard {...decorateProps(props)} />)
                                    : (<Redirect to={{ pathname: '/login', state: { from: props.location } }} />)
                            } />
                    </Switch>
                </div>
            </section>
        </Router>
    );
};
