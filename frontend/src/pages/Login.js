import React, { useRef } from 'react';
import "react-simple-flex-grid/lib/main.css";
import { doSignup } from '../redux/actions';

/*
    Component For Login Component
*/

export const Login = (props) => {
    const { appState, dispatch } = props;
    const loginRef = useRef(null);
    const passwordRef = useRef(null);
    

    
    const signup = () => {
        const enteredUsername = loginRef.current.value;
        const enteredPassword = passwordRef.current.value;
        doSignup(dispatch, { enteredUsername, enteredPassword });
    }

    if (appState.authState.loggedin) {
        props.history.push('/dashboard');
    }

    return (
        <section>
            <h2>Sign up</h2>
            <input type="text" ref={loginRef} placeholder="User name" />
            <input type="password" ref={passwordRef} placeholder="Password" />
            <button onClick={ signup }>Sign up</button>
            <label> Try <code>davyJones/n9G%YE6nnhWG</code> or <code>johnGibbs/P0$6nq4z2AKN</code> </label>
        </section>
    );
};