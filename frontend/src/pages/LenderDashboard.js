import React, { useEffect } from 'react';
import { Row, Col } from 'react-simple-flex-grid';
import "react-simple-flex-grid/lib/main.css";
import { getLoansForUser } from '../redux/actions';
import { TabularData } from '../components/TabularData';

/*
    Component For Lender Dashboard
*/
export const LenderDashboard = (props) => {
    const { appState, dispatch } = props;
    useEffect((() => {
        getLoansForUser(dispatch, { username: appState.authenticatedUser.username });
    }), []);

    console.log(appState);

    const heading = (
        <section>
            <h4>Welcome Mr. {appState.authenticatedUser.username}</h4>
            <h5>Your role is a lender, and here are all the loans you can fund</h5>
        </section>
    );

    const changeButtonIfNeeded = (loanStatus) => {
        if (loanStatus === 'funded') {
            return (<button disabled={true}> Done </button> )
        } else {
            return (<button> Fund </button> )
        }
    }

    const renderTableData = (data) => data.map((row, idx) => (<Row key={idx}>
        <Col span={2}>{row.loanId}</Col>
        <Col span={2}>{row.loanAmount}</Col>
        <Col span={3}>{row.loanName}</Col>
        <Col span={3}>{row.loanStatus}</Col>
        <Col span={2}>{ changeButtonIfNeeded( row.loanStatus ) }</Col>
    </Row>
    ));


    const renderTableHeadings = () => (
        <Row>
            <Col span={2}>Loan Id</Col>
            <Col span={2}>Amount</Col>
            <Col span={3}>Type</Col>
            <Col span={3}>Status</Col>
            <Col span={2}>Action</Col>
            
        </Row>
    );


    return (<div>
        <TabularData data={appState.loans} heading={heading} renderTableData={renderTableData(appState.loans)} renderTableHeadings={renderTableHeadings()} />
    </div>);
}