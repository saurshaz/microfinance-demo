import React from 'react';
import "react-simple-flex-grid/lib/main.css";
import { LenderDashboard } from './LenderDashboard';
import { BorrowerDashboard } from './BorrowerDashboard';
/*
    Component For Dashboard
*/
export const Dashboard = (props) => {
    const { appState, history } = props;

    if (appState.authState.loggedinuser) {
        history.push('/');
    }

    const bodyComponent = (appState.authenticatedUser.role !== 'borrower') ? (<LenderDashboard {...props} />) : (<BorrowerDashboard {...props}/>)
    return (<section> {bodyComponent} </section>);
}