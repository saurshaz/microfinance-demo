import React, { useEffect } from 'react';
import { Row, Col } from 'react-simple-flex-grid';
import "react-simple-flex-grid/lib/main.css";
import { getLoansForUser } from '../redux/actions';
import { TabularData } from '../components/TabularData';

/*
    Component For Borrower Dashboard
*/
export const BorrowerDashboard = (props) => {
    const { appState, dispatch } = props;
    useEffect((() => {
        getLoansForUser(dispatch, { username: appState.authenticatedUser.username });
    }), []);

    console.log(appState);


    const heading = (
        <section>
            <h4>Welcome Mr. {appState.authenticatedUser.username}</h4>
            <h5>Your role is a borrower, and here are all the loans you've requested</h5>
        </section>
    );


    const renderTableData = (data) => data.map((row, idx) => (<Row key={idx}>
        <Col span={3}>{row.loanId}</Col>
        <Col span={3}>{row.loanAmount}</Col>
        <Col span={3}>{row.loanName}</Col>
        <Col span={3}>{row.loanStatus}</Col>
    </Row>
    ));


    const renderTableHeadings = () => (
        <Row>
            <Col span={3}>Loan Id</Col>
            <Col span={3}>Amount</Col>
            <Col span={3}>Type</Col>
            <Col span={3}>Status</Col>
        </Row>
    );


    return (<div>
        <TabularData data={appState.loans} heading={heading} renderTableData={renderTableData(appState.loans)} renderTableHeadings={renderTableHeadings()} />
        <Row>
            <Col span={12}>
                <button> Request New </button>
            </Col>
        </Row>
    </div>);
}