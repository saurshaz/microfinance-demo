# ReactMicroFinance Boilerplate

ReactMicroFinance provides an integrated technology stack that

Demo is at https://build.microfinance-demo.now.sh/

* 📚 Contains a huge amount of best practices already implemented for you
* 🚀 Provides fast and powerful local application development. Including:
  hot-reloading, local utilities (npm scripts), dev tools (linters, git hooks),
  etc.

Some of the best practices include:

* 📦 Caching dependencies for faster builds
* 🚀 Fast track for faster deploys
* 🔒 Properly storage of secrets outside the code
* 💊 Healthcheck implementations
* ✒️ Linters and code prettifiers
* 🏛 Source code architecture that scales

Some of the used technologies are:

* [Node.js](https://nodejs.org/en/) for the backend.
* [React.js](https://reactjs.org/) for the frontend with many other libraries
  already integrated such as redux, react-router, redux-saga, etc.
* [Docker](https://www.docker.com) for containers.
* [Docker Compose](https://docs.docker.com/compose) to run containers locally
  and during the build.

Containers offer big advantages in software development, quality assurance and
software deployment -- namely consistency, reliability and scalability. In
particular, scalability is implemented from the beginning, and the system is
ready to grow as the application gets traction.

## Local Development

### Local development tools

First of all, run `yarn install` in the root directory. This will install local
development tools such as `eslint` and git precommits to keep the code formatted
and without obvious errors.

### Installation

Local prerequisites are minimal, please follow the
[installation instructions](INSTALL.md) carefully. We support Linux and MacOS;
Windows users can use Docker for Windows with several workarounds or use a Linux VM.
Review your Docker Advanced settings and consider to assign more CPUs and more memory
to the Docker process to boost performance.

### Running the application

```
$ docker/run
```

That's it! Your application is available at http://localhost:3000 (React frontend). frontend supports hot reloading.

> On first execution, Docker must download the base container images, which
> might take a while. Subsequent executions will be faster, taking advantage of
> Docker caching and Yarn caching. See [here](CACHING.md) for details about the
> caching mechanisms.

`docker/run` is the local development start script. This allows for changes made
locally to restart the node application in the most efficient and cross-platform
way. To configure the app restart, edit `nodemon.json`. `docker/run` uses
`docker-compose` to start the application and is probably how you will want to
do most of your development.

**NOTE:** Any change to `package.json` will require a full restart of the
container: you should use `CTRL+C` to stop the running Docker instance and
restart it to see your changes. To avoid that when doing simple changes (like
adding a package), you can do something like:

```
docker-compose run api yarn add $YOUR_PACKAGE$
```

### Customizing Style

The boilerplate supports styling with SASS/SCSS. Just edit `main.scss` on `frontend/src/styles` and the boilerplate will convert it to css on-the-fly so you can take full advantage of all of SASS's features.

### Tests

#### Running the tests

```
$ docker/test
```

#### Creating new tests

**Unit tests**

Just write regular tests using `jest`. Use `describe()` and `it()` to write new
tests and test suites.
